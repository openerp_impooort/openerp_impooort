#!/usr/bin/env ruby
# coding: utf-8

require 'rubygems'
require 'ooor'
require 'csv'
require 'ruby-rtf'

def import_partenaires fichier, num_ligne

	i = 0
	CSV.foreach(fichier, {:col_sep => ","}) {|ligne|
		if (i>0 and i>num_ligne) # Pour éviter la ligne d'en-tête du CSV et commencer à la ligne num_ligne
	
			partner = ResPartner.where(:name => ligne[1]).first
			if !partner then
				partner = ResPartner.new
				partner.name = ligne[1]
			end	
			partner.street = ligne[5]
			partner.street2 = ligne[6]
			partner.zip = ligne[7]
			partner.city = ligne[8]
			partner.phone = ligne[10]
			partner.fax = ligne[11]
			partner.mobile = ligne[12]
			partner.email = ligne[13]
			partner.street = ligne[5]
			partner.active = true
			partner.customer = true
			
			res = partner.save
	
			if res then
				puts "#{i} -> #{partner.name}"
				i += 1
			else
				puts "Erreur partenaire #{partner.name} : #{res.error}"
			end
		else
			i += 1
		end
	}

	puts "Nb de partenaires ajoutés : #{i}"
	
end

def import_articles fichier, num_ligne

	i = 0
	CSV.foreach(fichier, {:col_sep => ","}) {|ligne|
		if (i>0 and i>num_ligne) # Pour éviter la ligne d'en-tête du CSV et commencer à la ligne num_ligne
	
			product = ProductProduct.where(:name => ligne[3]).first
			if !product then
				product = ProductProduct.new
				product.name = ligne[3]
			end	
			product.description = rtf_to_text ligne[4]
			product.price = ligne[15]
			product.price_margin = ligne[18]
			product.available_in_pos = true
			product.active = ligne[36]
			product.sale_ok = true
			product_purchase_ok = true
			product.weight = ligne[31]
			product.qty_available = ligne[52]
			
			res = product.save
	
			if res then
				puts "#{i} -> #{product.name}"
				i += 1
			else
				puts "Erreur article #{product.name} : #{res.error}"
			end
		else
			i += 1
		end
	}

	puts "Nb d'articles ajoutés : #{i}"
	
end

def rtf_to_text rtf
	text = ""
	unless rtf.to_s.empty?
		begin
			doc = RubyRTF::Parser.new.parse(rtf)
			doc.sections.each do |section|
				ligne = section[:text].unpack('C*').pack('U*')
				text << "#{ligne.gsub("","")}\n"
			end
		rescue
			puts "Impossible de traduire le texte rtf"
		end
	end
	return text
end

def choix
	puts "Que souhaitez-vous faire ?"
	puts "1 => Importer les articles."
	puts "2 => Importer les clients."
	
	reponse = gets.chomp.to_i
	
	if [1,2].include?(reponse)
		puts "Commencer à partir de quelle ligne ? (par défaut la permière)"
		
		num_ligne = gets.chomp.to_i
		
		config = YAML.load_file("configuration.yaml")
		ooor_instance = Ooor.new(config["OpenERP"])
	
		import_articles config["CSV"]["articles"], num_ligne  if reponse==1
		import_partenaires config["CSV"]["clients"], num_ligne  if reponse==2
	end
end

choix
